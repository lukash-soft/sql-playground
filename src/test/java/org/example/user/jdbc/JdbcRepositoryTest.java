package org.example.user.jdbc;

import org.example.user.model.MyUser;
import org.example.user.repository.MyUserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

class JdbcRepositoryTest {


    final MyUserRepository repository = new JdbcRepository();

    @Test
    void smokeTest() {
        List<MyUser> usersBeforeAnyActions = repository.getAll();
        Assertions.assertEquals(0, usersBeforeAnyActions.size());

        MyUser firstSaved = repository.save(new MyUser.MyUserBuilder()
                .withLastName("Pietrynczak")
                .withFirstName("Lukasz")
                .build());

        List<MyUser> getAllUsersAfterUpdate = repository.getAll();
        Assertions.assertEquals(usersBeforeAnyActions.size() + 1, getAllUsersAfterUpdate.size());

        Optional<MyUser> userGotById = repository.getOneById(firstSaved.getId());
        Assertions.assertTrue(userGotById.isPresent());

        getAllUsersAfterUpdate.forEach(user -> repository.deleteUserById(user.getId()));

        List<MyUser> getAllUsersAfterDeletion = repository.getAll();
        Assertions.assertEquals(0, getAllUsersAfterDeletion.size());
    }

}
