package org.example.user.hibernate;

import org.example.user.model.MyUser;
import org.example.user.repository.DatabaseProperties;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class HibernateUtil {

    public static final SessionFactory SESSION_FACTORY;

    static {
        Configuration configuration = new Configuration();

        Properties settings = new Properties();

        settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
        settings.put(Environment.URL, DatabaseProperties.DB_URL);
        settings.put(Environment.USER, DatabaseProperties.DB_USERNAME);
        settings.put(Environment.PASS, DatabaseProperties.DB_PASSWORD);

        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
        settings.put(Environment.SHOW_SQL, "true");

        settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
        settings.put(Environment.HBM2DDL_AUTO, "update");

        configuration.setProperties(settings);
        configuration.addAnnotatedClass(MyUser.class);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();

        SESSION_FACTORY = configuration.buildSessionFactory(serviceRegistry);
    }
}
