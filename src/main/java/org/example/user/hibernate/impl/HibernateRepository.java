package org.example.user.hibernate.impl;

import org.example.user.hibernate.HibernateUtil;
import org.example.user.model.MyUser;
import org.example.user.repository.MyUserRepository;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Optional;

public class HibernateRepository implements MyUserRepository {

    @Override
    public List<MyUser> getAll() {
        try (Session session = HibernateUtil.SESSION_FACTORY.openSession()) {
            return session.createQuery("FROM MyUser", MyUser.class).list();
        }
    }

    @Override
    public Optional<MyUser> getOneById(Long id) {

        try (Session session = HibernateUtil.SESSION_FACTORY.openSession()) {
            MyUser userToGet = session.get(MyUser.class, id);
            return Optional.of(userToGet);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public MyUser save(MyUser myUser) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.SESSION_FACTORY.openSession()) {
            transaction = session.beginTransaction();
            session.persist(myUser);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
                e.printStackTrace();
            }
        }

        return myUser;
    }

    @Override
    public void deleteUserById(Long id) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.SESSION_FACTORY.openSession()) {
            transaction = session.beginTransaction();
            MyUser userToDelete = session.get(MyUser.class, id);
            session.remove(userToDelete);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
}
