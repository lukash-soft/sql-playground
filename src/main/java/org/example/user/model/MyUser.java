package org.example.user.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class MyUser {

    @Id
    @GeneratedValue
    private Long id;
    private String firstName;
    private String lastName;

    public MyUser(){

    }

    public MyUser(Long id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public static class MyUserBuilder {
        private Long id;
        private String firstName;
        private String lastName;

        public MyUserBuilder() {
        }

        public MyUserBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public MyUserBuilder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public MyUserBuilder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public MyUser build() {
            return new MyUser(
                    this.id,
                    this.firstName,
                    this.lastName
            );
        }
    }

}
