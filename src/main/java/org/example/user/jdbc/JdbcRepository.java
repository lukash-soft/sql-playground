package org.example.user.jdbc;

import org.example.user.model.MyUser;
import org.example.user.repository.DatabaseProperties;
import org.example.user.repository.MyUserRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class JdbcRepository implements MyUserRepository {
    private static String SELECT_ALL_QUERY = "SELECT * FROM myUser";
    private static final String SELECT_ONE_BY_ID = "SELECT * FROM myUser WHERE id = ?";
    public static final String SQL_DELETE_QUERY = "DELETE FROM myUser WHERE id = ?";
    public static final String INSERT_SQL = "INSERT INTO myuser(firstName, lastName) VALUES (?, ?)";

    public List<MyUser> getAll() {
        return getUsers(SELECT_ALL_QUERY);
    }

    private List<MyUser> getUsers(String sqlQuery) {
        try (Connection connection = createConnection();
             Statement preparedStatement = connection.prepareStatement(sqlQuery);
             ResultSet result = preparedStatement.executeQuery(SELECT_ALL_QUERY)) {

            List<MyUser> users = new ArrayList<>();
            while (result.next()) {
                MyUser myUser = new MyUser.MyUserBuilder()
                        .withId(result.getLong("id"))
                        .withFirstName(result.getString("firstName"))
                        .withLastName(result.getString("lastName"))
                        .build();
                users.add(myUser);
            }
            return users;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public Optional<MyUser> getOneById(Long id) {
        ResultSet result = null;

        try (Connection connection = createConnection();
             PreparedStatement pstmt = connection.prepareStatement(SELECT_ONE_BY_ID)) {
            pstmt.setLong(1, id);

            result = pstmt.executeQuery();

            if (result.next()) {
                MyUser myUser = new MyUser(result.getLong("id"), result.getString("firstName"), result.getString("lastName"));
                return Optional.of(myUser);
            }
            return Optional.empty();

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            closeResultSet(result);
        }
    }

    private void closeResultSet(ResultSet result) {
        try {
            if (result != null) {
                result.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(
                DatabaseProperties.DB_URL,
                DatabaseProperties.DB_USERNAME,
                DatabaseProperties.DB_PASSWORD);
    }

    public MyUser save(MyUser myUser) {
        ResultSet result = null;
        try (Connection con = createConnection();
             PreparedStatement pstmt = con.prepareStatement(INSERT_SQL, Statement.RETURN_GENERATED_KEYS)) {

            pstmt.setString(1, myUser.getFirstName());
            pstmt.setString(2, myUser.getLastName());
            pstmt.executeUpdate();

            result = pstmt.getGeneratedKeys();
            if (result.next()) {
                myUser.setId(result.getLong(1));
            }
            return myUser;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            closeResultSet(result);
        }
    }

    public void deleteUserById(Long id) {
        try (Connection connection = createConnection();
             PreparedStatement pstmt = connection.prepareStatement(SQL_DELETE_QUERY)) {

            pstmt.setLong(1, id);
            pstmt.executeUpdate();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
