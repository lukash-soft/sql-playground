package org.example.user.repository;

import org.example.user.model.MyUser;

import java.util.List;
import java.util.Optional;

public interface MyUserRepository {

    List<MyUser> getAll();
    Optional<MyUser> getOneById(Long id);
    MyUser save(MyUser myUser);
    void deleteUserById(Long id);

}
